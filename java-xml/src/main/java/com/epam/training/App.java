package com.epam.training;

import com.epam.training.builders.AbstractVoucherBuilder;
import com.epam.training.builders.DomVoucherBuilder;
import com.epam.training.builders.SaxVoucherBuilder;
import com.epam.training.data.TouristVoucher;
import com.epam.training.data.enums.ParserType;
import com.epam.training.data.enums.TransportType;
import com.epam.training.data.enums.VoucherType;
import com.epam.training.manager.VoucherParserManager;

/**
 * Epam - Java Web Development Training
 * Java - XML
 * Assel Sarzhanova
 */

public class App {

    public static void main(String[] args) {
        AbstractVoucherBuilder builder = VoucherParserManager.getInstance().createVoucherParserBuilder(ParserType.SAX);
        if (builder instanceof SaxVoucherBuilder) {
            SaxVoucherBuilder saxBuilder = (SaxVoucherBuilder) builder;
            saxBuilder.getVouchersWithConfirmationFlag(true)
                    .getVouchersWithType(VoucherType.HEALTH)
                    .buildVoucherSet("voucher.xml");
        } else if (builder instanceof DomVoucherBuilder) {
            DomVoucherBuilder domBuilder = (DomVoucherBuilder) builder;
            domBuilder.getVouchersWithTransportType(TransportType.AIR_TRANSPORT)
                    .getVouchersWithVoucherType(VoucherType.HOLIDAY)
                    .buildVoucherSet("voucher.xml");
        } else {
            builder.buildVoucherSet("voucher.xml");
        }

        for (TouristVoucher voucher : builder.getVoucherSet()) {
            System.out.println(voucher);
        }

        builder.writeVoucherSet("voucherOutput.xml");
    }
}
