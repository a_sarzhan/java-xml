package com.epam.training.manager;

import com.epam.training.builders.AbstractVoucherBuilder;
import com.epam.training.builders.DomVoucherBuilder;
import com.epam.training.builders.SaxVoucherBuilder;
import com.epam.training.builders.StaxVoucherBuilder;
import com.epam.training.data.enums.ParserType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VoucherParserManager {
    private static final VoucherParserManager MANAGER_INSTANCE = new VoucherParserManager();
    private static final Logger logger = LogManager.getLogger(VoucherParserManager.class);

    private VoucherParserManager() {
    }

    public static VoucherParserManager getInstance() {
        return MANAGER_INSTANCE;
    }

    public AbstractVoucherBuilder createVoucherParserBuilder(ParserType parser) {
        switch (parser) {
            case SAX:
                return new SaxVoucherBuilder();
            case DOM:
                return new DomVoucherBuilder();
            case STAX:
                return new StaxVoucherBuilder();
            default:
                logger.error("Error in voucher parser creation. Unknown parser name [" + parser.name() + "]");
                throw new EnumConstantNotPresentException(parser.getDeclaringClass(), parser.name());
        }

    }
}
