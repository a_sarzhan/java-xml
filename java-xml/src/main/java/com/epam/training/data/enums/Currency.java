package com.epam.training.data.enums;

public enum Currency {
    EUR, USD, RUB, KZT, RMB, GBP
}
