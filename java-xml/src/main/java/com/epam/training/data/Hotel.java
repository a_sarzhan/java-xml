package com.epam.training.data;

import com.epam.training.data.enums.BoardBasis;

public class Hotel {
    private String name;
    private int hotelStar;
    private BoardBasis boardBasis;
    private int roomType;
    private boolean withTv;
    private boolean withAirConditioner;
    private boolean withWifi;
    private boolean withSafe;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHotelStar() {
        return hotelStar;
    }

    public void setHotelStar(int hotelStar) {
        this.hotelStar = hotelStar;
    }

    public BoardBasis getBoardBasis() {
        return boardBasis;
    }

    public void setBoardBasis(BoardBasis boardBasis) {
        this.boardBasis = boardBasis;
    }

    public int getRoomType() {
        return roomType;
    }

    public void setRoomType(int roomType) {
        this.roomType = roomType;
    }

    public boolean isWithTv() {
        return withTv;
    }

    public void setWithTv(boolean withTv) {
        this.withTv = withTv;
    }

    public boolean isWithAirConditioner() {
        return withAirConditioner;
    }

    public void setWithAirConditioner(boolean withAirConditioner) {
        this.withAirConditioner = withAirConditioner;
    }

    public boolean isWithWifi() {
        return withWifi;
    }

    public void setWithWifi(boolean withWifi) {
        this.withWifi = withWifi;
    }

    public boolean isWithSafe() {
        return withSafe;
    }

    public void setWithSafe(boolean withSafe) {
        this.withSafe = withSafe;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "name='" + name + '\'' +
                ", hotelStar=" + hotelStar +
                ", boardBasis=" + boardBasis +
                ", roomType=" + roomType +
                ", withTv=" + withTv +
                ", withAirConditioner=" + withAirConditioner +
                ", withWifi=" + withWifi +
                ", withSafe=" + withSafe +
                '}';
    }
}
