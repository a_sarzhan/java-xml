package com.epam.training.data.enums;

public enum VoucherTag {
    TOURIST_VOUCHERS("touristVouchers"),
    VOUCHER("voucher"),
    ID("id"),
    TYPE("type"),
    CONFIRMATION("confirmation"),
    COUNTRY("country"),
    NUMBER_DAYS_AND_NIGHTS("numberDaysAndNights"),
    TRANSPORT("transport"),
    STAR("star"),
    BOARD_BASIS("boardBasis"),
    ROOM_TYPE("roomType"),
    AIR_CONDITIONER("airConditioner"),
    TV("tv"),
    WI_FI("wifi"),
    SAFE("safe"),
    HOTEL("hotel"),
    TRANSFER("transfer"),
    INSURANCE("insurance"),
    OTHER_SERVICE("otherService"),
    MEDICAL_SERVICE("medicalService"),
    VISA("visa"),
    TOTAL_COST("totalCost"),
    COST("cost"),
    CONDITION("condition"),
    HOTEL_CHARACTERISTIC("hotelCharacteristic"),
    NAME("name"),
    CURRENCY("currency");

    private String name;

    VoucherTag(String tagName) {
        this.name = tagName;
    }

    public String getName() {
        return name;
    }
}
