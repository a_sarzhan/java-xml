package com.epam.training.data;

import com.epam.training.data.enums.TransportType;
import com.epam.training.data.enums.VoucherType;

public class TouristVoucher {
    private String id;
    private boolean confirmation;
    private VoucherType type;
    private String country;
    private String daysAndNights;
    private TransportType transport;
    private Hotel hotel;
    private CostDetails costDetail;

    public TouristVoucher() {
    }

    public TouristVoucher(String id) {
        this.id = id;
        costDetail = new CostDetails();
        hotel = new Hotel();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isConfirmation() {
        return confirmation;
    }

    public void setConfirmation(boolean confirmation) {
        this.confirmation = confirmation;
    }

    public VoucherType getType() {
        return type;
    }

    public void setType(VoucherType type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDaysAndNights() {
        return daysAndNights;
    }

    public void setDaysAndNights(String daysAndNights) {
        this.daysAndNights = daysAndNights;
    }

    public TransportType getTransport() {
        return transport;
    }

    public void setTransport(TransportType transport) {
        this.transport = transport;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public CostDetails getCostDetail() {
        return costDetail;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public void setCostDetail(CostDetails costDetail) {
        this.costDetail = costDetail;
    }

    @Override
    public String toString() {
        return "TouristVoucher{" +
                "id='" + id + '\'' +
                ", confirmation=" + confirmation +
                ", type=" + type +
                ", country='" + country + '\'' +
                ", daysAndNights='" + daysAndNights + '\'' +
                ", transport=" + transport +
                ", hotel=" + hotel +
                ", costDetail=" + costDetail +
                '}';
    }
}
