package com.epam.training.data;

import com.epam.training.data.enums.Currency;

public class CostDetails {
    private Currency currency;
    private double hotelCost;
    private double transferCost;
    private double insuranceCost;
    private double medicalServiceCost;
    private double otherServiceCost;
    private double visaCost;
    private double totalCost;

    public double getHotelCost() {
        return hotelCost;
    }

    public void setHotelCost(double hotelCost) {
        this.hotelCost = hotelCost;
    }

    public double getTransferCost() {
        return transferCost;
    }

    public void setTransferCost(double transferCost) {
        this.transferCost = transferCost;
    }

    public double getInsuranceCost() {
        return insuranceCost;
    }

    public void setInsuranceCost(double insuranceCost) {
        this.insuranceCost = insuranceCost;
    }

    public double getMedicalServiceCost() {
        return medicalServiceCost;
    }

    public void setMedicalServiceCost(double medicalServiceCost) {
        this.medicalServiceCost = medicalServiceCost;
    }

    public double getOtherServiceCost() {
        return otherServiceCost;
    }

    public void setOtherServiceCost(double otherServiceCost) {
        this.otherServiceCost = otherServiceCost;
    }

    public double getVisaCost() {
        return visaCost;
    }

    public void setVisaCost(double visaCost) {
        this.visaCost = visaCost;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "CostDetails{" +
                "currency=" + currency +
                ", hotelCost=" + hotelCost +
                ", transferCost=" + transferCost +
                ", insuranceCost=" + insuranceCost +
                ", medicalServiceCost=" + medicalServiceCost +
                ", otherServiceCost=" + otherServiceCost +
                ", visaCost=" + visaCost +
                ", totalCost=" + totalCost +
                '}';
    }
}
