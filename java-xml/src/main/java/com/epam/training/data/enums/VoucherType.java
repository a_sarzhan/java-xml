package com.epam.training.data.enums;

public enum VoucherType {
    HOLIDAY, EXCURSION, HEALTH, PILGRIMAGE
}
