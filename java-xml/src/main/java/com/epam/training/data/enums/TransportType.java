package com.epam.training.data.enums;

public enum TransportType {
    AIR_TRANSPORT, RAIL_TRANSPORT, ROAD_TRANSPORT, WATER_TRANSPORT
}
