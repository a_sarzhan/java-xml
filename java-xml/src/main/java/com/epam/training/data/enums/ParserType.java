package com.epam.training.data.enums;

public enum ParserType {
    SAX, DOM, STAX
}
