package com.epam.training.utility;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

public class FormatFiles {
    private static final Logger logger = LogManager.getLogger(FormatFiles.class);

    public static File getXmlFile(String inputFileName) {
        File xmlFile = null;
        try {
            URL resource = Objects.requireNonNull(FormatFiles.class.getClassLoader().getResource(inputFileName));
            xmlFile = new File(resource.toURI());
        } catch (URISyntaxException e) {
            logger.error("Error occurred while accessing xml file named =" + inputFileName + ". Details: " + e.getMessage());
            e.printStackTrace();
        }
        return xmlFile;
    }
}
