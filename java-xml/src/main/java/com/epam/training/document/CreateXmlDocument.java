package com.epam.training.document;

import com.epam.training.data.CostDetails;
import com.epam.training.data.Hotel;
import com.epam.training.data.TouristVoucher;
import com.epam.training.data.enums.VoucherTag;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public class CreateXmlDocument {
    private static final Logger logger = LogManager.getLogger(CreateXmlDocument.class);
    private Document document;
    private static final CreateXmlDocument INSTANCE = new CreateXmlDocument();

    private CreateXmlDocument() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.newDocument();
        } catch (ParserConfigurationException e) {
            logger.error("Error in CreateXmlDocument constructor, while creating document. Details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static CreateXmlDocument getInstance() {
        if (INSTANCE == null) {
            return new CreateXmlDocument();
        }
        return INSTANCE;
    }

    public void formXmlResultDocument(Set<TouristVoucher> vouchers, String outputFile) {
        String root = VoucherTag.TOURIST_VOUCHERS.getName();
        Element rootElement = document.createElement(root);
        document.appendChild(rootElement);

        for (TouristVoucher voucher : vouchers) {
            String entityName = VoucherTag.VOUCHER.getName();
            Element entityElement = document.createElement(entityName);
            entityElement.setAttribute(VoucherTag.ID.getName(), voucher.getId());
            entityElement.setAttribute(VoucherTag.CONFIRMATION.getName(), Boolean.toString(voucher.isConfirmation()));
            entityElement.setAttribute(VoucherTag.TYPE.getName(), voucher.getType().name().toLowerCase());
            createVoucherInnerElements(entityElement, voucher);
            rootElement.appendChild(entityElement);
        }

        writeVouchersToXMLFile(outputFile);
    }

    private void writeVouchersToXMLFile(String outputFile) {
        TransformerFactory factory = TransformerFactory.newInstance();
        try {
            Transformer transformer = factory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new FileWriter(outputFile));
            transformer.transform(source, result);
            logger.info("All data from voucher set has been successfully transformed to xml file [" + outputFile + "]");
        } catch (IOException | TransformerException e) {
            logger.error("Error occurred while transforming xml document. Details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void createVoucherInnerElements(Element voucherElement, TouristVoucher voucher) {
        Element countryTag = getElement(VoucherTag.COUNTRY.getName(), voucher.getCountry());
        Element durationTag = getElement(VoucherTag.NUMBER_DAYS_AND_NIGHTS.getName(), voucher.getDaysAndNights());
        String transportValue = voucher.getTransport().name().toLowerCase().replaceAll("_", "-");
        Element transportTag = getElement(VoucherTag.TRANSPORT.getName(), transportValue);

        String hotel = VoucherTag.HOTEL_CHARACTERISTIC.getName();
        Element hotelTag = document.createElement(hotel);
        hotelTag.setAttribute(VoucherTag.NAME.getName(), voucher.getHotel().getName());
        createHotelElements(hotelTag, voucher.getHotel());

        Element costTag = document.createElement(VoucherTag.COST.getName());
        costTag.setAttribute(VoucherTag.CURRENCY.getName(), voucher.getCostDetail().getCurrency().name());
        createCostElements(costTag, voucher.getCostDetail());

        voucherElement.appendChild(countryTag);
        voucherElement.appendChild(durationTag);
        voucherElement.appendChild(transportTag);
        voucherElement.appendChild(hotelTag);
        voucherElement.appendChild(costTag);
    }

    private void createHotelElements(Element tag, Hotel hotel) {
        Element starTag = getElement(VoucherTag.STAR.getName(), Integer.toString(hotel.getHotelStar()));
        Element basisTag = getElement(VoucherTag.BOARD_BASIS.getName(), hotel.getBoardBasis().name());
        Element roomTag = getElement(VoucherTag.ROOM_TYPE.getName(), Integer.toString(hotel.getRoomType()));
        Element conditionTag = document.createElement(VoucherTag.CONDITION.getName());

        String value;
        value = (hotel.isWithAirConditioner()) ? "yes" : "no";
        Element airConditionerTag = getElement(VoucherTag.AIR_CONDITIONER.getName(), value);
        value = (hotel.isWithTv()) ? "yes" : "no";
        Element tvTag = getElement(VoucherTag.TV.getName(), value);
        value = (hotel.isWithWifi()) ? "yes" : "no";
        Element wifiTag = getElement(VoucherTag.WI_FI.getName(), value);
        value = (hotel.isWithSafe()) ? "yes" : "no";
        Element safeTag = getElement(VoucherTag.SAFE.getName(), value);

        conditionTag.appendChild(airConditionerTag);
        conditionTag.appendChild(tvTag);
        conditionTag.appendChild(wifiTag);
        conditionTag.appendChild(safeTag);
        tag.appendChild(starTag);
        tag.appendChild(basisTag);
        tag.appendChild(roomTag);
        tag.appendChild(conditionTag);
    }

    private void createCostElements(Element tag, CostDetails cost) {
        Element hotelTag = getElement(VoucherTag.HOTEL.getName(), Double.toString(cost.getHotelCost()));
        tag.appendChild(hotelTag);
        Element transferTag = getElement(VoucherTag.TRANSFER.getName(), Double.toString(cost.getTransferCost()));
        tag.appendChild(transferTag);
        Element insuranceTag = getElement(VoucherTag.INSURANCE.getName(), Double.toString(cost.getInsuranceCost()));
        tag.appendChild(insuranceTag);

        if (cost.getOtherServiceCost() != 0) {
            Element otherTag = getElement(VoucherTag.OTHER_SERVICE.getName(), Double.toString(cost.getOtherServiceCost()));
            tag.appendChild(otherTag);
        }
        if (cost.getMedicalServiceCost() != 0) {
            Element medicalTag = getElement(VoucherTag.MEDICAL_SERVICE.getName(), Double.toString(cost.getMedicalServiceCost()));
            tag.appendChild(medicalTag);
        }
        if (cost.getVisaCost() != 0) {
            Element visaTag = getElement(VoucherTag.VISA.getName(), Double.toString(cost.getVisaCost()));
            tag.appendChild(visaTag);
        }
        Element totalTag = getElement(VoucherTag.TOTAL_COST.getName(), Double.toString(cost.getTotalCost()));
        tag.appendChild(totalTag);
    }

    private Element getElement(String elementName, String elementText) {
        Element element = document.createElement(elementName);
        element.appendChild(document.createTextNode(elementText));
        return element;
    }
}
