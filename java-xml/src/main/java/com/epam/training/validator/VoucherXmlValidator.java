package com.epam.training.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

public class VoucherXmlValidator {
    private static final String VOUCHER_SCHEMA = "voucherSchema.xsd";
    private static final Logger logger = LogManager.getLogger(VoucherXmlValidator.class);

    public static boolean validate(File xmlFile) {
        try {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            URL source = VoucherXmlValidator.class.getClassLoader().getResource(VOUCHER_SCHEMA);
            Schema schema = schemaFactory.newSchema(new File(Objects.requireNonNull(source).toURI()));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlFile));
        } catch (SAXException | IOException e) {
            logger.error("Validation Fail. xml file [" + xmlFile.getName() + "], schema [" + VOUCHER_SCHEMA
                    + "]. Details: " + e.getMessage());
            return false;
        } catch (URISyntaxException e) {
            logger.error("Validation Fail. Error occurred while getting xml schema [" + VOUCHER_SCHEMA
                    + "]. Details: " + e.getMessage());
            return false;
        }
        logger.info("Validation Success. xml file [" + xmlFile.getName() + "], schema [" + VOUCHER_SCHEMA + "]");
        return true;
    }
}
