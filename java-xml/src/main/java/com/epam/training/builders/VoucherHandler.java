package com.epam.training.builders;

import com.epam.training.data.TouristVoucher;
import com.epam.training.data.enums.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

class VoucherHandler extends DefaultHandler {
    private static final Logger logger = LogManager.getLogger(VoucherHandler.class);
    private Set<TouristVoucher> voucherSet = new HashSet<>();
    private String idFlag;
    private Boolean confirmationFlag;
    private VoucherType typeFlag;
    private TouristVoucher voucher;
    private VoucherTag thisTag;
    private final EnumSet<VoucherTag> tagSet = EnumSet.range(VoucherTag.COUNTRY, VoucherTag.TOTAL_COST);

    VoucherHandler(SaxVoucherBuilder builder) {
        this.idFlag = builder.getIdFlag();
        this.confirmationFlag = builder.getConfirmationFlag();
        this.typeFlag = builder.getTypeFlag();
    }

    Set<TouristVoucher> getVoucherSet() {
        return voucherSet;
    }

    private void initializeVoucherBasedOnFlags(String id, boolean confirmed, VoucherType type) {
        if ((idFlag == null || id.equals(idFlag))
                && (confirmationFlag == null || confirmationFlag.equals(confirmed))
                && (typeFlag == null || type.equals(typeFlag))) {
            voucher = new TouristVoucher(id);
            voucher.setConfirmation(confirmed);
            voucher.setType(type);
            logger.info("Tourist voucher with id=" + voucher.getId() + " is created");
        }
    }

    private void setCurrentNecessaryElementTag(String elementName) {
        for (VoucherTag tag : VoucherTag.values()) {
            if (tag.getName().equalsIgnoreCase(elementName) && tagSet.contains(tag)) {
                thisTag = tag;
            }
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equals("voucher")) {
            String id = attributes.getValue("id");
            boolean confirmation = Boolean.parseBoolean(attributes.getValue("confirmation"));
            VoucherType type = (attributes.getValue("type") == null) ? VoucherType.HOLIDAY
                    : VoucherType.valueOf(attributes.getValue("type").toUpperCase());
            initializeVoucherBasedOnFlags(id, confirmation, type);
        }

        if (voucher == null) return;

        if (qName.equals("hotelCharacteristic")) {
            voucher.getHotel().setName(attributes.getValue("name"));
        } else if (qName.equals("cost")) {
            Currency currency = (attributes.getValue("currency") == null) ? Currency.USD
                    : Currency.valueOf(attributes.getValue("currency"));
            voucher.getCostDetail().setCurrency(currency);
        } else {
            setCurrentNecessaryElementTag(qName);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("voucher") && voucher != null) {
            voucherSet.add(voucher);
            logger.info("Tourist voucher with id=" + voucher.getId() + " is added to vouchers set");
            voucher = null;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        String value = new String(ch, start, length).trim();
        if (thisTag != null) {
            switch (thisTag) {
                case COUNTRY:
                    voucher.setCountry(value);
                    break;
                case NUMBER_DAYS_AND_NIGHTS:
                    voucher.setDaysAndNights(value);
                    break;
                case TRANSPORT:
                    voucher.setTransport(TransportType.valueOf(value.toUpperCase().replaceAll("-", "_")));
                    break;
                default:
                    assignHotelDetails(value);
            }
            thisTag = null;
        }
    }

    private void assignHotelDetails(String elementValue) {
        switch (thisTag) {
            case STAR:
                voucher.getHotel().setHotelStar(Integer.parseInt(elementValue));
                break;
            case BOARD_BASIS:
                voucher.getHotel().setBoardBasis(BoardBasis.valueOf(elementValue.toUpperCase()));
                break;
            case ROOM_TYPE:
                voucher.getHotel().setRoomType(Integer.parseInt(elementValue));
                break;
            case AIR_CONDITIONER:
                voucher.getHotel().setWithAirConditioner(elementValue.equals("yes"));
                break;
            case TV:
                voucher.getHotel().setWithTv(elementValue.equals("yes"));
                break;
            case WI_FI:
                voucher.getHotel().setWithWifi(elementValue.equals("yes"));
                break;
            case SAFE:
                voucher.getHotel().setWithSafe(elementValue.equals("yes"));
                break;
            default:
                assignCostDetails(elementValue);

        }
    }

    private void assignCostDetails(String elementValue) {
        switch (thisTag) {
            case HOTEL:
                voucher.getCostDetail().setHotelCost(Double.parseDouble(elementValue));
                break;
            case TRANSFER:
                voucher.getCostDetail().setTransferCost(Double.parseDouble(elementValue));
                break;
            case INSURANCE:
                voucher.getCostDetail().setInsuranceCost(Double.parseDouble(elementValue));
                break;
            case OTHER_SERVICE:
                voucher.getCostDetail().setOtherServiceCost(Double.parseDouble(elementValue));
                break;
            case MEDICAL_SERVICE:
                voucher.getCostDetail().setMedicalServiceCost(Double.parseDouble(elementValue));
                break;
            case VISA:
                voucher.getCostDetail().setVisaCost(Double.parseDouble(elementValue));
                break;
            case TOTAL_COST:
                voucher.getCostDetail().setTotalCost(Double.parseDouble(elementValue));
                break;
            default:
                throw new EnumConstantNotPresentException(thisTag.getDeclaringClass(), thisTag.name());
        }
    }

}
