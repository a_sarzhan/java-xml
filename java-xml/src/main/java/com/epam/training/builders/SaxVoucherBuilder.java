package com.epam.training.builders;

import com.epam.training.data.enums.VoucherType;
import com.epam.training.utility.FormatFiles;
import com.epam.training.validator.VoucherXmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class SaxVoucherBuilder extends AbstractVoucherBuilder {
    private static final Logger logger = LogManager.getLogger(SaxVoucherBuilder.class);
    private Boolean confirmationFlag;
    private String idFlag;
    private VoucherType typeFlag;
    private SAXParser saxParser;

    public SaxVoucherBuilder() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            saxParser = saxParserFactory.newSAXParser();
        } catch (ParserConfigurationException | SAXException e) {
            logger.error("Error in SaxVoucherBuilder constructor while getting sax parser. " + e.getMessage());
            e.printStackTrace();
        }
    }

    public SaxVoucherBuilder getVouchersWithConfirmationFlag(boolean confirmation) {
        this.confirmationFlag = confirmation;
        logger.info("vouchers with confirmation flag [" + confirmation + "] will be retrieved");
        return this;
    }

    public SaxVoucherBuilder getVoucherById(String id) {
        this.idFlag = id;
        logger.info("voucher with id flag [" + id + "] will be retrieved");
        return this;
    }

    public SaxVoucherBuilder getVouchersWithType(VoucherType type) {
        this.typeFlag = type;
        logger.info("vouchers with type [" + type + "] will be retrieved");
        return this;
    }

    public SaxVoucherBuilder getAllVouchers() {
        this.typeFlag = null;
        this.idFlag = null;
        this.confirmationFlag = null;
        logger.info("all vouchers will be retrieved");
        return this;
    }

    Boolean getConfirmationFlag() {
        return confirmationFlag;
    }

    String getIdFlag() {
        return idFlag;
    }

    VoucherType getTypeFlag() {
        return typeFlag;
    }

    public void buildVoucherSet(String fileName) {
        xmlFile = FormatFiles.getXmlFile(fileName);
        if (!VoucherXmlValidator.validate(xmlFile)) {
            return;
        }
        VoucherHandler handler = new VoucherHandler(this);
        try {
            saxParser.parse(xmlFile, handler);
        } catch (SAXException | IOException e) {
            logger.error("SaxBuilder. Error occurred while building voucher set from [" + xmlFile + "]. Details: " + e.getMessage());
            e.printStackTrace();
        }
        voucherSet = handler.getVoucherSet();
    }

}
