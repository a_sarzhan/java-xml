package com.epam.training.builders;

import com.epam.training.data.TouristVoucher;
import com.epam.training.document.CreateXmlDocument;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractVoucherBuilder {
    Set<TouristVoucher> voucherSet;
    File xmlFile;

    AbstractVoucherBuilder() {
        this.voucherSet = new HashSet<>();
    }

    public Set<TouristVoucher> getVoucherSet() {
        return voucherSet;
    }

    public abstract void buildVoucherSet(String fileName);

    public void writeVoucherSet(String outputFile) {
        CreateXmlDocument.getInstance().formXmlResultDocument(voucherSet, outputFile);
    }
}
