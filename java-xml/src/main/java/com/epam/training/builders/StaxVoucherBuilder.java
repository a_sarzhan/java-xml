package com.epam.training.builders;

import com.epam.training.data.CostDetails;
import com.epam.training.data.Hotel;
import com.epam.training.data.TouristVoucher;
import com.epam.training.data.enums.*;
import com.epam.training.utility.FormatFiles;
import com.epam.training.validator.VoucherXmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;

public class StaxVoucherBuilder extends AbstractVoucherBuilder {
    private XMLInputFactory inputFactory;
    private static final Logger logger = LogManager.getLogger(StaxVoucherBuilder.class);

    public StaxVoucherBuilder() {
        inputFactory = XMLInputFactory.newFactory();
    }

    @Override
    public void buildVoucherSet(String fileName) {
        xmlFile = FormatFiles.getXmlFile(fileName);
        if (!VoucherXmlValidator.validate(xmlFile)) {
            return;
        }
        String entityName;
        try (FileInputStream inputStream = new FileInputStream(xmlFile)) {
            XMLStreamReader xmlReader = inputFactory.createXMLStreamReader(inputStream);
            while (xmlReader.hasNext()) {
                int type = xmlReader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    entityName = xmlReader.getLocalName();
                    if (VoucherTag.VOUCHER.equals(getTagName(entityName))) {
                        TouristVoucher touristVoucher = buildVoucher(xmlReader);
                        voucherSet.add(touristVoucher);
                        logger.info("voucher with id=" + touristVoucher.getId() + " is added to voucher set");
                    }
                }
            }
        } catch (XMLStreamException | IOException e) {
            logger.error("StaxBuilder. Error occurred while building voucher set from file [" + xmlFile
                    + "]. Details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void assignVoucherAttributes(TouristVoucher voucher, XMLStreamReader reader) {
        voucher.setId(reader.getAttributeValue(null, VoucherTag.ID.getName()));
        boolean confirmed = Boolean.parseBoolean(reader.getAttributeValue(null, VoucherTag.CONFIRMATION.getName()));
        voucher.setConfirmation(confirmed);
        if (reader.getAttributeValue(null, VoucherTag.TYPE.getName()) == null) {
            voucher.setType(VoucherType.HOLIDAY);
        } else {
            String type = reader.getAttributeValue(null, VoucherTag.TYPE.getName()).toUpperCase();
            voucher.setType(VoucherType.valueOf(type));
        }
    }

    private TouristVoucher buildVoucher(XMLStreamReader reader) throws XMLStreamException {
        TouristVoucher voucher = new TouristVoucher();
        assignVoucherAttributes(voucher, reader);
        String elementName;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    elementName = reader.getLocalName();
                    initializeVoucherInnerElements(reader, elementName, voucher);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    elementName = reader.getLocalName();
                    if (VoucherTag.VOUCHER.equals(getTagName(elementName))) {
                        return voucher;
                    }
                    break;
            }
        }
        logger.error("Unknown element in tag [" + VoucherTag.VOUCHER.getName() + "]");
        throw new XMLStreamException("Unknown element");
    }

    private VoucherTag getTagName(String elementName) throws XMLStreamException {
        for (VoucherTag tag : VoucherTag.values()) {
            if (tag.getName().equals(elementName.trim())) {
                return tag;
            }
        }
        throw new XMLStreamException("Unknown element");
    }

    private void initializeVoucherInnerElements(XMLStreamReader reader, String name, TouristVoucher voucher) throws XMLStreamException {
        switch (getTagName(name)) {
            case COUNTRY:
                voucher.setCountry(getElementText(reader));
                break;
            case NUMBER_DAYS_AND_NIGHTS:
                voucher.setDaysAndNights(getElementText(reader));
                break;
            case TRANSPORT:
                String formattedString = getElementText(reader).replaceAll("-", "_").toUpperCase();
                voucher.setTransport(TransportType.valueOf(formattedString));
                break;
            case HOTEL_CHARACTERISTIC:
                voucher.setHotel(getXmlHotel(reader));
                break;
            case COST:
                voucher.setCostDetail(getXmlCostDetails(reader));
                break;
        }

    }

    private Hotel getXmlHotel(XMLStreamReader reader) throws XMLStreamException {
        Hotel hotel = new Hotel();
        hotel.setName(reader.getAttributeValue(null, VoucherTag.NAME.getName()));
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    assignHotelFields(name, reader, hotel);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (VoucherTag.HOTEL_CHARACTERISTIC.getName().equals(name)) {
                        return hotel;
                    }
                    break;
            }
        }
        logger.error("StaxParser. Unknown element in tag [" + VoucherTag.HOTEL_CHARACTERISTIC.getName() + "]");
        throw new XMLStreamException("Unknown element in tag hotelCharacteristic");
    }

    private void assignHotelFields(String name, XMLStreamReader reader, Hotel hotel) throws XMLStreamException {
        switch (getTagName(name)) {
            case STAR:
                hotel.setHotelStar(Integer.parseInt(getElementText(reader)));
                break;
            case ROOM_TYPE:
                hotel.setRoomType(Integer.parseInt(getElementText(reader)));
                break;
            case BOARD_BASIS:
                hotel.setBoardBasis(BoardBasis.valueOf(getElementText(reader)));
                break;
            case WI_FI:
                hotel.setWithWifi(getElementText(reader).equals("yes"));
                break;
            case SAFE:
                hotel.setWithSafe(getElementText(reader).equals("yes"));
                break;
            case TV:
                hotel.setWithTv(getElementText(reader).equals("yes"));
                break;
            case AIR_CONDITIONER:
                hotel.setWithAirConditioner(getElementText(reader).equals("yes"));
                break;
        }
    }

    private CostDetails getXmlCostDetails(XMLStreamReader reader) throws XMLStreamException {
        CostDetails cost = new CostDetails();
        Currency currency;
        if (reader.getAttributeValue(null, VoucherTag.CURRENCY.getName()) == null) {
            currency = Currency.USD;
        } else {
            currency = Currency.valueOf(reader.getAttributeValue(null, VoucherTag.CURRENCY.getName()));
        }
        cost.setCurrency(currency);
        String name;
        int type;

        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    assignCostDetailsFields(name, reader, cost);
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (VoucherTag.COST.getName().equals(name)) {
                        return cost;
                    }
                    break;
            }
        }
        logger.error("StaxParser. Unknown element in tag [" + VoucherTag.COST.getName() + "]");
        throw new XMLStreamException("Unknown element in tag cost");
    }

    private void assignCostDetailsFields(String name, XMLStreamReader reader, CostDetails cost) throws XMLStreamException {
        switch (getTagName(name)) {
            case TRANSFER:
                cost.setTransferCost(Double.parseDouble(getElementText(reader)));
                break;
            case INSURANCE:
                cost.setInsuranceCost(Double.parseDouble(getElementText(reader)));
                break;
            case VISA:
                cost.setVisaCost(Double.parseDouble(getElementText(reader)));
                break;
            case MEDICAL_SERVICE:
                cost.setMedicalServiceCost(Double.parseDouble(getElementText(reader)));
                break;
            case HOTEL:
                cost.setHotelCost(Double.parseDouble(getElementText(reader)));
                break;
            case OTHER_SERVICE:
                cost.setOtherServiceCost(Double.parseDouble(getElementText(reader)));
                break;
            case TOTAL_COST:
                cost.setTotalCost(Double.parseDouble(getElementText(reader)));
                break;
        }
    }

    private String getElementText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }
}
