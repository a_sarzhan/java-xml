package com.epam.training.builders;

import com.epam.training.data.CostDetails;
import com.epam.training.data.Hotel;
import com.epam.training.data.TouristVoucher;
import com.epam.training.data.enums.*;
import com.epam.training.utility.FormatFiles;
import com.epam.training.validator.VoucherXmlValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DomVoucherBuilder extends AbstractVoucherBuilder {
    private static final Logger logger = LogManager.getLogger(DomVoucherBuilder.class);
    private DocumentBuilder documentBuilder;
    private Map<String, String> flagMap = new HashMap<>();
    private TouristVoucher voucher;

    public DomVoucherBuilder() {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            logger.error("Error in DomVoucherBuilder constructor. Details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public DomVoucherBuilder getVouchersWithTransportType(TransportType transport) {
        flagMap.put("transport", transport.name());
        logger.info("vouchers with transport type [" + transport + "] will be retrieved");
        return this;
    }

    public DomVoucherBuilder getVouchersWithCountry(String country) {
        flagMap.put("country", country);
        logger.info("vouchers with country [" + country + "] will be retrieved");
        return this;
    }

    public DomVoucherBuilder getVouchersWithHotelName(String hotelName) {
        flagMap.put("hotelName", hotelName);
        logger.info("vouchers with hotel name [" + hotelName + "] will be retrieved");
        return this;
    }

    public DomVoucherBuilder getVouchersWithVoucherType(VoucherType type) {
        flagMap.put("type", type.name());
        logger.info("vouchers with voucher type [" + type + "] will be retrieved");
        return this;
    }

    public DomVoucherBuilder getVoucherWithId(String id) {
        flagMap.put("id", id);
        logger.info("voucher with id [" + id + "] will be retrieved");
        return this;
    }

    public DomVoucherBuilder getVouchersWithConfirmationFlag(boolean confirmed) {
        flagMap.put("confirmation", Boolean.toString(confirmed));
        logger.info("vouchers with confirmation flag [" + confirmed + "] will be retrieved");
        return this;
    }

    public void buildVoucherSet(String fileName) {
        xmlFile = FormatFiles.getXmlFile(fileName);
        if (!VoucherXmlValidator.validate(xmlFile)) {
            return;
        }
        try {
            Document document = documentBuilder.parse(xmlFile);
            Element root = document.getDocumentElement();
            NodeList voucherList = root.getElementsByTagName(VoucherTag.VOUCHER.getName());
            fillVoucherSet(voucherList);
        } catch (SAXException | IOException e) {
            logger.error("DomBuilder. Error occurred while building voucher set from file [" + xmlFile
                    + "]. Details: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void fillVoucherSet(NodeList voucherList) {
        for (int i = 0; i < voucherList.getLength(); i++) {
            Element voucherElement = (Element) voucherList.item(i);
            TouristVoucher voucher = buildTouristVoucher(voucherElement);
            if (isApplicableVoucher()) {
                voucherSet.add(voucher);
                logger.info("voucher with id=" + voucher.getId() + " is added to voucher set");
            }
        }
    }

    private boolean isApplicableVoucher() {
        boolean accept;
        for (Map.Entry<String, String> entry : flagMap.entrySet()) {
            switch (entry.getKey()) {
                case "transport":
                    accept = voucher.getTransport().equals(TransportType.valueOf(entry.getValue()));
                    break;
                case "country":
                    accept = voucher.getCountry().equalsIgnoreCase(entry.getValue());
                    break;
                case "type":
                    accept = voucher.getType().equals(VoucherType.valueOf(entry.getValue()));
                    break;
                case "hotelName":
                    accept = voucher.getHotel().getName().equalsIgnoreCase(entry.getValue());
                    break;
                case "id":
                    accept = voucher.getId().equalsIgnoreCase(entry.getValue());
                    break;
                case "confirmation":
                    accept = (voucher.isConfirmation() == Boolean.parseBoolean(entry.getValue()));
                    break;
                default:
                    accept = true;
            }

            if (!accept) {
                return false;
            }
        }
        return true;
    }

    private TouristVoucher buildTouristVoucher(Element voucherElement) {
        voucher = new TouristVoucher();
        voucher.setId(voucherElement.getAttribute(VoucherTag.ID.getName()));
        voucher.setConfirmation(Boolean.parseBoolean(voucherElement.getAttribute("confirmation")));
        if (voucherElement.getAttribute("type").isEmpty()) {
            voucher.setType(VoucherType.HOLIDAY);
        } else {
            voucher.setType(VoucherType.valueOf(voucherElement.getAttribute("type").toUpperCase()));
        }
        voucher.setCountry(getChildElementValue(voucherElement, VoucherTag.COUNTRY.getName()));
        voucher.setDaysAndNights(getChildElementValue(voucherElement, VoucherTag.NUMBER_DAYS_AND_NIGHTS.getName()));
        String transportValue = getChildElementValue(voucherElement, "transport").toUpperCase();
        TransportType transport = TransportType.valueOf(transportValue.replaceAll("-", "_"));
        voucher.setTransport(transport);
        voucher.setHotel(buildVoucherHotelDetails(voucherElement));
        voucher.setCostDetail(buildVoucherCostDetail(voucherElement));
        return voucher;
    }

    private static String getChildElementValue(Element element, String childElementName) throws NullPointerException {
        NodeList childList = element.getElementsByTagName(childElementName);
        Node childElement = childList.item(0);
        return childElement.getTextContent();
    }

    private Hotel buildVoucherHotelDetails(Element voucherElement) {
        Hotel hotel = new Hotel();
        String hotelCharacteristic = VoucherTag.HOTEL_CHARACTERISTIC.getName();
        Element hotelElement = (Element) voucherElement.getElementsByTagName(hotelCharacteristic).item(0);
        hotel.setName(hotelElement.getAttribute(VoucherTag.NAME.getName()));

        String hotelStar = getChildElementValue(hotelElement, VoucherTag.STAR.getName());
        String roomType = getChildElementValue(hotelElement, VoucherTag.ROOM_TYPE.getName());
        String boardBasis = getChildElementValue(hotelElement, VoucherTag.BOARD_BASIS.getName());
        String airConditioner = getChildElementValue(hotelElement, VoucherTag.AIR_CONDITIONER.getName());
        String wifi = getChildElementValue(hotelElement, VoucherTag.WI_FI.getName());
        String tv = getChildElementValue(hotelElement, VoucherTag.TV.getName());
        String safe = getChildElementValue(hotelElement, VoucherTag.SAFE.getName());

        hotel.setHotelStar(Integer.parseInt(hotelStar));
        hotel.setRoomType(Integer.parseInt(roomType));
        hotel.setBoardBasis(BoardBasis.valueOf(boardBasis));
        hotel.setWithAirConditioner(airConditioner.equals("yes"));
        hotel.setWithWifi(wifi.equals("yes"));
        hotel.setWithTv(tv.equals("yes"));
        hotel.setWithSafe(safe.equals("yes"));
        return hotel;
    }

    private CostDetails buildVoucherCostDetail(Element voucherElement) {
        CostDetails cost = new CostDetails();
        Element costElement = (Element) voucherElement.getElementsByTagName(VoucherTag.COST.getName()).item(0);
        if (costElement.getAttribute(VoucherTag.CURRENCY.getName()).isEmpty()) {
            cost.setCurrency(Currency.USD);
        } else {
            cost.setCurrency(Currency.valueOf(costElement.getAttribute(VoucherTag.CURRENCY.getName())));
        }
        String hotelCost = getChildElementValue(costElement, VoucherTag.HOTEL.getName());
        String transferCost = getChildElementValue(costElement, VoucherTag.TRANSFER.getName());
        String totalCost = getChildElementValue(costElement, VoucherTag.TOTAL_COST.getName());
        String insuranceCost = getChildElementValue(costElement, VoucherTag.INSURANCE.getName());
        cost.setHotelCost(Double.parseDouble(hotelCost));
        cost.setTransferCost(Double.parseDouble(transferCost));
        cost.setTotalCost(Double.parseDouble(totalCost));
        cost.setInsuranceCost(Double.parseDouble(insuranceCost));
        setOptionalCostsCausingNullPointerException(costElement, cost);
        return cost;
    }

    private void setOptionalCostsCausingNullPointerException(Element costElement, CostDetails cost) {
        try {
            String otherServiceCost = getChildElementValue(costElement, VoucherTag.OTHER_SERVICE.getName());
            cost.setOtherServiceCost(Double.parseDouble(otherServiceCost));
        } catch (NullPointerException ignore) {
            logger.info("Optional xml element [" + VoucherTag.OTHER_SERVICE.getName() + "] is skipped");
        }
        try {
            String medicalServiceCost = getChildElementValue(costElement, VoucherTag.MEDICAL_SERVICE.getName());
            cost.setMedicalServiceCost(Double.parseDouble(medicalServiceCost));
        } catch (NullPointerException ignore) {
            logger.info("Optional xml element [" + VoucherTag.MEDICAL_SERVICE.getName() + "] is skipped");
        }
        try {
            String visaCost = getChildElementValue(costElement, VoucherTag.VISA.getName());
            cost.setVisaCost(Double.parseDouble(visaCost));
        } catch (NullPointerException ignore) {
            logger.info("Optional xml element [" + VoucherTag.VISA.getName() + "] is skipped");
        }
    }

}
